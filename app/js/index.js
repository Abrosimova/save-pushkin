// import { Cookie } from './modules/cookie.js'
import { API } from './modules/api.js'
import { Auth } from './modules/auth.js'
import { appOptions } from './modules/settings.js'
import { Loader } from './modules/loader.js'
// import { Activity } from './modules/activity.js'
import { GroupRating } from './modules/groupRating.js'
import { Content } from './modules/content.js'
import { Test } from './modules/test.js'
import { Timer } from './modules/timer.js'
import { wordForm } from './modules/common.js'
import { Modal } from './modules/modal.js'

/* global ga */

var App = {
  container: '.main-container',
  init: function() {
    var that = this;

    API.init();
    if (!appOptions.isMobile && document.location.href.indexOf('access_token') > -1) {
      that.drawAuthPage();
      Auth.auth(function() {
        var path = window.location.href.substring(0, window.location.href.indexOf('?'));

        history.pushState('', document.title, path);
        window.opener.location.reload();
        window.close();
      });

    } else if (document.location.href.indexOf('access_token') > -1) {
      Auth.auth(function () {
        var path = window.location.href.substring(0, window.location.href.indexOf('?'));

        history.pushState('', document.title, path);
        that.drawMain();
      });
    } else {
      this.drawMain();
    }
  },

  drawAuthPage: function() {
    var html = '<div class="auth-container">';

    html += '<div class="auth-container__text">Это окно закроется через несколько секунд</div>'
    html += '</div>'
    $('body').empty().html(html);
  },

  drawMain: function () {
    var html = '';
      // that = this;

    html += '<div class="main-bg">'
    html += appOptions.provider.indexOf('mos') === -1 ? '<a href="https://dnevnik.ru" target="_blank"><img src="' + appOptions.cdnPath + 'dnevnik-logo.png" alt="" class="dn-logo"></a>' : ''
    html += '<img src="' + appOptions.cdnPath + 'leaves-left.png" alt="" class="leaves leaves--left">'
    html += '<img src="' + appOptions.cdnPath + 'leaves-right.png" alt="" class="leaves leaves--right">'
    html += '<img src="' + appOptions.cdnPath + 'main.png?' + new Date().getTime() + '" alt="" class="main-img">'
    html += '<button class="main-btn"><span class="main-btn__text">Смотреть трейлер</span></button>'
    var primiere = 1493240400645,
      today = new Date().getTime();

    if (today > primiere) {
      html += '<a target="_blank" href="' + appOptions.forumLink + '">'
      html += '<button class="forum-btn">Внимание! Конкурс!</button></a>'
    }
    html += '</div>'
    html += '<div class="main-content">'
    html += '<div class="main-text main-text--large">Что если Пушкин не погиб на дуэли?</div>'
    html += '<div class="main-text">Ученики из обычной московской школы в решающий момент смертельного сражения '
    html += 'перенесли поэта в XXI век! И хотя пуля Дантеса не достигла цели, современный мир таит в себе не '
    html += 'меньше опасностей. Смотри в кино семейную комедию «Спасти Пушкина» уже с 27 апреля!</div>'
    html += '<button class="start">Победители игры</button>'
    html += '<div class="desc">Участвуй в квесте по мотивам произведений Пушкина, чтобы проверить знания</div>'
    html += '<div class="rules">'
    html += '<span class="rules-text">Полные правила</span>'
    html += '</div>'
    html += '<img src="' + appOptions.cdnPath + 'footer.png" class="footer"/>'
    html += '</div>'

    // function LazyLoad() {
    //   if (API.model.user.roles.indexOf('EduStudent') > -1) {
    //     API.getKeysFromDB({
    //       label: appOptions.provider + '-groupRating',
    //       pageSize: 100,
    //       pageNumber: 1
    //     }, function (data) {
    //       if (data.Paging.next) {
    //         API.getRecursivePage(data.Paging.next, function (data) {
    //           that.top = data;
    //         });
    //       } else {
    //         that.top = data;
    //       }
    //     })
    //   } else {
    //     API.getKeysFromDB({
    //       label: appOptions.provider + '-activity',
    //       pageSize: 100,
    //       pageNumber: 1
    //     }, function (data) {
    //       if (data.Paging.next) {
    //         API.getRecursivePage(data.Paging.next, function (data) {
    //           that.top = data;
    //         });
    //       } else {
    //         that.top = data;
    //       }
    //     })
    //   }
    // }

    $(this.container).empty().html(html);
    $(this.container).css('background', '#d5c4b8');

    $('.start').on('click', function () {
      ga('send', 'event', 'Кнопка', 'Играть');
      var html = '';

      html += '<div class="modal-title modal-title--small modal-title--win">Победители конкурса «СПАСТИ ПУШКИНА»</div>'
      html += '<div class="win-container">'
      if (appOptions.provider.indexOf('mosreg') > -1) {
        html += '<div class="win-item">'
        html += '<img class="win-item__img" width="140" height="140" src="https://static.school.mosreg.ru/images/avatars/user/a.l.jpg">'
        html += '<div class="win-item__name">Куровское, Куровская СОШ №1, 3-а</div>'
        html += '</div>'
        html += '<div class="win-item">'
        html += '<img class="win-item__img" width="140" height="140" src="http://au1.school.mosreg.ru/get.aspx/36/f29bb1688870492dab0dc4c869f8948d.l.jpg?d=20151002091636">'
        html += '<div class="win-item__name">'
        html += '<a class="win-item__link" target="_blank" href="https://school.mosreg.ru/user/user.aspx?user=1000004687634">Некрасова Светлана Викторовна'
        html += '</a></div></div>'
        html += '<div class="win-item">'
        html += '<img class="win-item__img" width="140" height="140" src="https://static.school.mosreg.ru/images/avatars/user/a.l.jpg">'
        html += '<div class="win-item__name">'
        html += '<a class="win-item__link" target="_blank" href="https://school.mosreg.ru/user/user.aspx?user=1000004027856">Кочергина Елена Анатольевна'
        html += '</a></div></div>'
      } else {
        html += '<div class="win-item">'
        html += '<img class="win-item__img" width="140" height="140" src="https://static.school.mosreg.ru/images/avatars/user/a.l.jpg">'
        html += '<div class="win-item__name">Воронеж, МБОУ Лицей № 8, 4-б</div>'
        html += '</div>'
        html += '<div class="win-item">'
        html += '<img class="win-item__img" width="140" height="140" src="https://b6.csdnevnik.ru/user-avatar/4acb7f55a83ae0119cef0019bbd299a0.l.jpg">'
        html += '<div class="win-item__name">'
        html += '<a class="win-item__link" target="_blank" href="https://dnevnik.ru/user/user.aspx?user=411520">Беляевская Татьяна Яновна</a></div>'
        html += '</div>'
        html += '<div class="win-item">'
        html += '<img class="win-item__img" width="140" height="140" src="https://b3.csdnevnik.ru/user-avatar/9b2b3cea4f6543be9032c3024c624b3d.l.jpg">'
        html += '<div class="win-item__name">'
        html += '<a class="win-item__link" target="_blank" href="https://dnevnik.ru/user/user.aspx?user=1000001226935">Галина Николаевна Валенюк</a></div>'
        html += '</div>'
      }
      html += '</div>'
      html += '<button class="start close-modal">Закрыть</button>'
      Modal.open(html);

      $('.close-modal').on('click', function() {
        Modal.close();
      })
      // if (void 0 !== Cookie.get('token')) {
      //   API.token = Cookie.get('token');
      //   Loader.show();
      //   API.getUserInfo(function() {
      //     LazyLoad();
      //     Loader.show();
      //     that.activity = new Activity();
      //     that.activity.get(function() {
      //       Loader.hide();
      //       that.startTest();
      //     })
      //   });
      // } else if (document.location.search.indexOf('auth') === -1) {
      //   Auth.auth();
      // }
    });

    $('.rules-text').on('click', function() {
      var html = '';

      html += '<img class="modal-close" src="' + appOptions.cdnPath + 'close.png">'
      html += '<div class="rules-container">' + Content.getRules() + '</div>'
      Modal.open(html);
    });

    $('.main-btn').on('click', function () {
      var html = '';

      ga('send', 'event', 'Кнопка', 'Смотреть');
      html += '<img class="modal-close" src="' + appOptions.cdnPath + 'close.png">'
      html += '<iframe width="700" height="411" src="https://www.youtube.com/embed/Nq0jE2XJrUM?autoplay=1&rel=0" frameborder="0"'
      html += ' allowfullscreen></iframe>';

      Modal.open(html);
      $('.modal-content').css('padding', '10px 64px');
      $('.modal').css('width', 'auto')
    })
  },

  startTest: function() {
    var that = this,
      nextTestId = that.activity.getNextTestId(),
      segment = API.model.user.roles.indexOf('EduStudent') > -1 ? 1 : 2;

    if (nextTestId > -1) {
      if (void 0 !== that.activity.model.again && void 0 === that.again) {
        that.drawResult(that.activity.getLastResult());

        return;
      }
      Test.init(nextTestId, Content.getTest(nextTestId, segment), function (result) {
        if (void 0 === that.activity.model.again) {
          that.activity.model.test.push(result);
          if (void 0 !== that.activity.model.group) {
            that.groupRating = new GroupRating();
            that.groupRating.get(that.activity.model.group, function() {
              that.groupRating.model.rating += result.score;
              that.groupRating.model.time += result.time;
              that.groupRating.set();
            });
          }
          that.activity.set();
        } else {
          that.activity.model.againTest.push(result);
          that.activity.set();
        }
        that.drawResult(result);
      })
    } else {
      that.drawResult(that.activity.getLastResult());
    }
  },

  drawResult: function(result) {
    var html = '',
      that = this;

    html += '<div class="modal-title modal-title--small">Твой результат:</div>'
    if (result.id === 1) {
      html += '<div class="result-title">' + result.time + ' ' + wordForm.get('second', result.time) + '</div>'
    } else if (result.id === 2 || result.id === 3) {
      html += '<div class="result-title">' + result.score + '/10</div>'
    } else {
      html += '<div class="result-title">' + result.score + ' ' + wordForm.get('score', result.score) + '</div>'
    }
    if (result.id === 4) {
      html += '<div class="result-title result-title--small">Молодец! Ты справился со всеми заданиями!</div>'
      html += '<button class="start start-test">Перейти к результатам</button>'
      html += '<br><button class="start start-again">Пройти квест ещё раз!</button>'
      html += '<br><div class="cancel">Вернуться на главную</div>'
    } else {
      html += '<div class="result-title result-title--small">Ты хорошо справляешься! Готов к следующему заданию?</div>'
      html += '<button class="start start-test">Продолжить</button>'
      html += void 0 !== that.activity.model.again ? '<button class="start start-test" data-again="1">Перейти к результатам</button>' : '';
      html += '<br><div class="cancel">Отложить на потом</div>'
    }
    that.drawMain();
    Modal.open(html);

    $('.start-test').on('click', function () {
      if (result.id === 4 || void 0 !== $(this).data('again')) {
        Loader.show();
        var counter = 0
        var timer = setInterval(function () {
          counter++;

          if (void 0 !== that.top || counter > 200) {
            Loader.hide();
            clearInterval(timer);
            if (counter > 200) {
              alert('Произошла ошибка загрузки данных. Пропобуйте позже')
            } else {
              if ( API.model.user.roles.indexOf('EduStudent') > -1) {
                that.groupRating = new GroupRating();
                Loader.show();
                that.groupRating.get(that.activity.model.group, function() {
                  Loader.hide();
                  Modal.close();
                  that.drawRating();
                })
              } else {
                Modal.close();
                that.drawRating();
              }
            }
          }
        }, 300);
      } else {
        that.startTest();
      }
    });

    $('.cancel').on('click', function () {
      Modal.close();
      delete that.again;
    });

    $('.start-again').on('click', function () {
      that.again = 1;
      that.activity.model.again = 1;
      that.activity.model.againTest = [];
      that.activity.set();
      that.startTest();
    });
  },

  drawRating: function() {
    var data = this.top,
      top = [],
      i, json,
      html = '',
      that = this;

    for (i = 0; i < data.Keys.length; i++) {
      if (data.Keys[i].Key.split(appOptions.provider + '-activity_user')[1] == API.model.user.id_str) {
        top.push(getRating(that.activity.model))
      } else if (data.Keys[i].Key.split(appOptions.provider + '-groupRating_')[1] == that.activity.model.group && void 0 !== that.activity.model.group) {
        top.push(getRating(that.groupRating.model))
      } else if (API.model.user.roles.indexOf('EduStudent') > -1) {
        json = JSON.parse(data.Keys[i].Value);
        if (!json.school) {
          continue;
        }
        if (json.rating > 0) {
          top.push(getRating(json))
        }
      } else {
        json = JSON.parse(data.Keys[i].Value);
        if (json.test.length > 0 && void 0 === json.group) {
          top.push(getRating(json))
        }
      }
    }

    function getRating(data) {
      var time = 0,
        rating = 0,
        result = {},
        i;

      if (API.model.user.roles.indexOf('EduStudent') > -1) {
        result.school = data.school;
        result.time = data.time;
        result.score = data.rating;
      } else {
        data.test = data.test.slice(-4);
        for (i = 0; i < data.test.length; i++) {
          rating += data.test[i].score;
          time += data.test[i].time;
        }
        data.invite = data.invite.slice(-5);
        rating += data.invite.length;
        result.name = data.firstName + ' ' + data.lastName;
        result.id = data.id;
        result.time = time;
        result.score = rating;
      }

      return result;
    }

    top.sort(function(a, b) {
      if (a.score > b.score) {
        return -1;
      } else if (a.score < b.score) {
        return 1;
      } else if (a.time > b.time) {
        return 1;
      } else if (a.time < b.time) {
        return -1;
      }

      return 0;
    })
    function getPos() {
      for (var i = 0; i < top.length; i++) {
        if (API.model.user.roles.indexOf('EduStudent') > -1) {
          if (top[i].id === API.model.user.id_str) {
            return ++i;
          }
        } else {
          if (top[i].id === API.model.user.id_str) {
            return ++i;
          }
        }
      }
    }
    var pos = getPos();

    html += '<div class="rating-container">'
    html += appOptions.provider.indexOf('mos') === -1 ? '<a href="https://dnevnik.ru" target="_blank"><img src="' + appOptions.cdnPath + 'dn-logo-color.png" alt="" class="dn-logo"></a>' : ''
    html += '<div class="rating-title">Ты прошел все испытания'
    html += void 0 !== pos ? ',<br>твой результат <span class="it">' + pos + '-е место</span>!' : '';
    html += '</div>'
    html += '<img class="state state-1" src="' + appOptions.cdnPath + 'state1.png">'
    html += '<img class="state state-2" src="' + appOptions.cdnPath + 'state2.png">'
    if (API.model.user.roles.indexOf('EduStudent') > -1) {
      html += '<table class="result-table-header"><tr><td width="70%">Класс</td>'
      html += '<td>Баллы</td><td>Время</td></tr></table>'
    } else {
      html += '<table class="result-table-header"><tr><td width="70%">Участник</td>'
      html += '<td>Баллы</td><td>Время</td></tr></table>'
    }
    html += '<div class="rating-table-container">'
    html += '<table class="result-table">'
    html += drawTr(0)
    html += '</table>'
    if (top.length > 50) {
      html += '<button class="start load" data-step="1">Загрузить ещё</button>'
    }
    html += '</div>'
    html += that.activity.model.invite.length < 5 ? '<button class="start invite">Пригласить друзей</button>' : ''
    html += '<br><span class="it go-main">Вернуться на главную</span>'
    html += '</div>'
    $(that.container).empty().html(html);
    $(this.container).css('background', '#fff');

    $('.go-main').on('click', function() {
      that.drawMain();
    });

    $('.load').on('click', function() {
      var step = $(this).data('step');

      $('.result-table tbody').append(drawTr(step));
      $(this).data('step', step + 1);
    })

    $('.invite').on('click', function () {
      Loader.show();
      API.getFriends(function() {
        Loader.hide();
        that.drawFriendList();
      })
    });

    function drawTr(step) {
      var len = top.length > 50 * step + 50 ? 50 * step + 50 : top.length,
        html = '';

      for (i = 50 * step; i < len; i++) {
        // html += '<tr><td class="friend-photo"><img class="user-photo-img" width="50" height="50" src="'
        // html += data[i].photoMedium + '"></td>'
        html += '<tr'
        if (top[i].id == API.model.user.id_str && API.model.user.roles.indexOf('EduStudent') === -1) {
          html += ' class="rating-me"'
        }
        html += '>'
        html += '<td class="friend-check">' + (i + 1) + '. </td>'
        html += '<td class="rating-name">'
        if (API.model.user.roles.indexOf('EduStudent') === -1) {
          html += '<a target="_blank" href="' + appOptions.userLink + top[i].id + '" class="user-link">'
          html += top[i].name + '</a></td>'
        } else {
          if (top[i].school) {
            html += (top[i].school.school.city || '')
            html += ', ' + (top[i].school.school.name || '')
            html += ', ' + (top[i].school.group.fullName || '')
          }
        }
        html += '<td class="rating-res">' + top[i].score + '</td>'
        html += '<td class="rating-res rating-time">' + Timer.formatTime(top[i].time) + '</td></tr>'
      }
      if (len === top.length) {
        $('.load').remove();
      }

      return html;
    }
  },

  drawFriendList: function() {
    var html = '',
      that = this,
      data = API.model.friendsProfiles || [];

    html += '<img class="modal-close" src="' + appOptions.cdnPath + 'close.png">'
    html += '<div class="modal-title modal-title--small">Выбери пять друзей, которым хочешь'
    html += '<br>отправить приглашение:</div>'
    html += '<div class="friends-container">'
    for (var i = 0; i < data.length; i++) {
      if (that.activity.model.invite.indexOf(data[i].id_str) > -1) {
        continue;
      }
      html += '<div class="friend-item" data-id="' + data[i].id_str + '">'
      html += '<img class="user-photo" width="100" height="100" src="'
      html += data[i].photoMedium + '">'
      html += '<div class="friend-name">' + data[i].firstName + ' ' + data[i].lastName + '</div>'
      html += '</div>'
    }
    if (data.length === 0) {
      html += '<div class="modal-title modal-title--small">Не найдено ни одного друга</div>'
    }
    html += '</div>'
    html += data.length > 0 ? '<button class="start send-invite">Отправить</button>' : '';
    html += '</div>'
    html += '</div>'

    Modal.open(html);

    $('.modal-close').on('click', function() {
      Modal.close();
    });

    $('.friend-item').on('click', function () {
      if ($(this).hasClass('friend-item--selected')) {
        $(this).removeClass('friend-item--selected');
        $(this).children('.user-photo').removeClass('user-photo--selected');
      } else {
        if ($('.friend-item--selected').length + that.activity.model.invite.length === 5) {
          return;
        }
        $(this).addClass('friend-item--selected');
        $(this).children('.user-photo').addClass('user-photo--selected');
      }

    })

    $('.send-invite').on('click', function() {
      var ids = [],
        ans = [],
        wall = {
          body: 'Присоединяйся ко мне в проекте «Спасти Пушкина»! <a href="' + window.location.href + '" target="_blank">Участвовать</a>',
          from: API.model.user.id,
          from_str: API.model.user.id_str
        };

      $('.friend-item--selected').each(function() {
        ids.push(String($(this).data('id')))
      })

      if (ids.length === 0) {
        return;
      }
      Loader.show();
      $('.invite').prop('disabled', true);
      for (var i = 0; i < ids.length; i++) {
        wall.to = ids[i];
        wall.to_str = ids[i];
        API.sendMessage(wall, function() {
          ans.push(ids[i]);
          if (ans.length === ids.length) {
            sendDone();
            Loader.hide();
          }
        })
      }

      function sendDone() {
        that.activity.model.invite = that.activity.model.invite.concat(ids);
        // that.activity.model.rating += ids.length;
        that.groupRating = new GroupRating();
        that.groupRating.get(that.activity.model.group, function () {
          that.groupRating.model.rating += ids.length;
          that.groupRating.set();
        })
        that.activity.set();
        $('.invite').html('Приглашения отправлены');
      }
    })
  },
}

$(document).ready(function() {
  App.init();
})

// export { App }
