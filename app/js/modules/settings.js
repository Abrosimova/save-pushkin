var isMobile = (function() {
  if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i)
  || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)
  || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i)
  || navigator.userAgent.match(/Windows Phone/i)) {
    return true;
  }

  return false;
})();

var locationUrl = document.location.href,
  appOptions;

if (locationUrl.indexOf('school.mosreg') > -1) {
  appOptions = {
    authUrl: 'https://login.school.mosreg.ru/oauth2',
    grantUrl: 'https://api.school.mosreg.ru/v1/authorizations',
    scope: 'CommonInfo,EducationalInfo,Messages,FriendsAndRelatives',
    clientId: '3dac89bc34ca4852b452e54fb2fde692',
    redirectUrl: window.location.href + '/?auth=true',
    provider: 'mosreg-pushkin',
    api: 'https://api.school.mosreg.ru/v1/',
    isMobile: isMobile,
    userLink: 'https://school.mosreg.ru/user/user.aspx?user=',
    cdnPath: 'https://ad.csdnevnik.ru/special/staging/save-pushkin/css/img/',
    forumLink: 'https://ad.school.mosreg.ru/promo/save-pushkin-forum'
  }
} else {
  appOptions = {
    authUrl: 'https://login.dnevnik.ru/oauth2',
    grantUrl: 'https://api.dnevnik.ru/v1/authorizations',
    scope: 'CommonInfo,EducationalInfo,Messages,FriendsAndRelatives',
    clientId: 'd2ec9ae33e6148be90af95547db897bb',
    redirectUrl: window.location.href + '?auth=true',
    provider: 'pushkin',
    api: 'https://api.dnevnik.ru/v1/',
    isMobile: isMobile,
    userLink: 'https://dnevnik.ru/user/user.aspx?user=',
    // cdnPath: './dist/css/img/'
    cdnPath: 'https://ad.csdnevnik.ru/special/staging/save-pushkin/css/img/',
    forumLink: 'https://ad.dnevnik.ru/promo/save-pushkin-forum'
  }
}

export { appOptions }
