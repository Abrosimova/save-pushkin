import { API } from './api.js'
import { appOptions } from './settings.js'
// import { Activity } from './activity.js'

var GroupRating = function() {
  this.model = {}
}

GroupRating.prototype = {
  constructor: GroupRating,
  model: {},

  get: function(eduGroup, callback) {
    var that = this;

    this.eduGroup = eduGroup;
    API.getKeyFromDB({ key: appOptions.provider + '-groupRating_' + that.eduGroup }, function(data) {
      if (data) {
        var json = JSON.parse(data.Value);

        json.users = json.users || [API.model.user.id_str];
        that.model = json;
        // getActivity(0, json);
        if (typeof callback === 'function') {
          callback();
        }
      } else {
        that.model.users = [API.model.user.id_str];
        that.model.rating = 0;
        that.model.time = 0;
        API.getGroupFullInfo(that.eduGroup, function(data) {
          that.model.school = data;
          if (typeof callback === 'function') {
            callback();
          }
          // that.set(function () {
          //   if (typeof callback === 'function') {
          //     callback();
          //   }
          // });
        })
      }
    });
    // function getActivity(i, json) {
    //   var activity = new Activity({ api: API }),
    //     answer = [];
    //
    //   activity.get(function() {
    //     activity.model.id = json.users[i];
    //     answer.push(activity.model);
    //     if (answer.length === json.users.length) {
    //       that.create(answer, callback);
    //     } else {
    //       i++;
    //       getActivity(i);
    //     }
    //   }, json.users[i]);
    // }
  },

  // create: function(data, callback) {
  //   this.model.rating = 0;
  //   for (var i = 0; i < data.length; i++) {
  //     // if (data[i].id == '1000004934922' || data[i].id == '1000004285730') {
  //     //   this.model.users.splice(this.model.users.indexOf(data[i].id), 1);
  //     //   continue;
  //     // }
  //     this.model.rating += data[i].rating;
  //   }
  //   // this.model.rating = Math.ceil(this.model.rating / data.length);
  //   if (typeof callback === 'function') {
  //     callback();
  //   }
  // },

  set: function(callback) {
    var that = this;

    API.addKeyToDB({
      label: appOptions.provider + '-groupRating',
      key: appOptions.provider + '-groupRating_' + that.eduGroup,
      value: JSON.stringify(that.model)
    }, function() {
      if (typeof callback === 'function') {
        callback();
      }
    });
  }
}

export { GroupRating }
