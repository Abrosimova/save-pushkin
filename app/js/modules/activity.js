import { appOptions } from './settings.js'
import { API } from './api.js'
import { Content } from './content.js'

var Activity = function() {
  this.model = {};
}

Activity.prototype = {
  constructor: Activity,
  get: function(callback) {
    var that = this;

    API.getKeyFromDB({ key: appOptions.provider + '-activity_user' + API.model.user.id_str }, function(data) {
      // data = null;
      if (data) {
        that.model = JSON.parse(data.Value);
        that.model.questions = that.model.questions || [];
        if (typeof callback === 'function') {
          callback(data === null);
        }
      } else {
        that.model = {
          test: [],
          invite: [],
          rating: 0,
          firstName: API.model.user.firstName,
          lastName: API.model.user.lastName,
          id: API.model.user.id_str
        };
        if (API.model.user.roles.indexOf('EduStudent') > -1) {
          API.getMainGroup(function (data) {
            that.model.group = data.id_str
            if (typeof callback === 'function') {
              callback();
            }
          })
        } else {
          if (typeof callback === 'function') {
            callback();
          }
        }
      }
    });
  },

  getNextTestId: function() {
    var testIds = [],
      i, flag,
      list;

    if (void 0 === this.model.again) {
      list = this.model.test;
    } else {
      this.model.againTest = this.model.againTest.slice(-4)
      list = this.model.againTest;
    }
    for (i = 0; i < Content.test.length; i++) {
      testIds.push(Content.test[i].id)
    }
    for (i = 0; i < testIds.length; i++) {
      flag = false;
      for (var j = 0; j < list.length; j++) {
        if (list[j].id === testIds[i]) {
          flag = true;
          break;
        }
      }
      if (!flag) {
        return testIds[i];
      }
    }

    return -1;
  },

  getLastResult: function() {
    for (var i = 0; i < this.model.test.length; i++) {
      if (this.model.test[i].id === 4) {
        return this.model.test[i]
      }
    }
  },

  set: function(callback) {
    var that = this;

    API.addKeyToDB({
      label: appOptions.provider + '-activity',
      key: appOptions.provider + '-activity_user' + API.model.user.id_str,
      value: JSON.stringify(that.model)
    }, function() {
      if (typeof callback === 'function') {
        callback();
      }
    });
  }
}

export { Activity }
