import { wordForm } from './common.js'

var Timer = {
  container: '.timer',
  reverse: 0,
  start: function() {
    var that = this;

    if (this.reverse === 1) {
      this.time = this.maxTime;
    } else {
      this.time = 0;
    }

    this.timer = setInterval(function() {
      var val = $(that.container).html(),
        time = that.formatTime(that.time);

      if (that.reverse === 1) {
        if (that.time === 0) {
          that.stop(that.callback);
        } else {
          if (val !== time) {
            $(that.container).html(time);
          }
          that.time--;
        }
      } else {
        if (that.time === that.maxTime) {
          that.stop(that.callback);
        } else {
          if (val !== time) {
            $(that.container).html(time);
          }
          that.time++;
        }
      }
    }, 1000);

  },

  stop: function() {
    clearInterval(this.timer);
    if (typeof this.callback === 'function') {
      this.callback(this.time);
    }
  },

  formatTime: function(time) {
    var minutes = 0,
      seconds = 0,
      result;

    if (time / 86400000 > 1) {
      return Math.ceil(time / 86400000) + ' ' + wordForm.get('day', Math.ceil(time / 86400000));
    } else if (time < 60) {
      time = (time < 10) ? '0' + time : time;
      result = '00:' + time;
    } else {
      minutes = Math.floor(time / 60);
      minutes = (minutes < 10) ? '0' + minutes : minutes;
      seconds = Math.abs(Math.floor(time - minutes * 60));
      seconds = (seconds < 10) ? '0' + seconds : seconds;
      result = minutes + ':' + seconds;
    }

    return result;
  },

  getTime: function() {
    return this.time;
  }
}

export { Timer }
