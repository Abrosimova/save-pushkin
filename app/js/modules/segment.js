import { API } from './api.js'

var Segment = {
  model: [
    {
      id: 1,
      maxGroup: 5,
      minGroup: 1,
      type: 'child'
    },
    {
      id: 2,
      maxGroup: 8,
      minGroup: 6,
      type: 'child'
    },
    {
      id: 3,
      maxGroup: 11,
      minGroup: 9,
      type: 'child'
    },
    {
      id: 4,
      type: 'adult'
    }
  ],

  getSegment: function(callback) {
    var that = this;

    if (API.model.user.roles.indexOf('EduStudent') > -1) {
      API.getMainGroup(function(group) {
        for (var i = 0; i < that.model.length; i++) {
          if (group.parallel < that.model[i].maxGroup) {
            if (typeof callback === 'function') {
              callback(that.model[i].id);

              return;
            }
          }
        }
        if (typeof callback === 'function') {
          callback(3)

          return;
        }
      })
    } else {
      if (typeof callback === 'function') {
        callback(4)

        return;
      }
    }
  }
}

export { Segment }
