import { Timer } from './timer.js'
import { appOptions } from './settings.js'
import { Content } from './content.js'
import { Modal } from './modal.js'

var Test = {
  init: function(id, data, callback) {
    this.currentQuestion = -1;
    this.score = 0;
    this.model = data;
    this.test = Content.getTestById(id);
    this.callback = callback;
    this.start(id);
  },

  start: function (id) {
    var that = this;

    switch (id) {
      case 1:
        that.drawHiddenObject();
        break;
      case 2:
      case 3:
        that.drawTest();
        break;
      case 4:
        that.drawPortrets();
        break;
    }
  },

  drawTestModal: function(callback) {
    var html = '';

    html += '<div class="modal-title modal-title--small">Задание №' + this.test.id + '</div>'
    html += '<div class="modal-title">' + this.test.text + '</div>'
    html += '<button class="start start-test">Начать</button>'
    html += '<br><div class="cancel">Отложить на потом</div>'
    Modal.open(html);

    $('.start-test').on('click', function () {
      if (typeof callback === 'function') {
        callback();
      }
    });

    $('.cancel').on('click', function() {
      Modal.close();
    })
  },

  drawHiddenObject: function() {
    var that = this,
      html = '';

    html += '<div class="hidden-object-container">'
    html += appOptions.provider.indexOf('mos') === -1 ? '<img src="' + appOptions.cdnPath + 'dnevnik-logo.png" alt="" class="dn-logo">' : ''
    html += '<div class="counter">0/5</div>'
    html += '<div class="pic-container">'
    html += '<div class="pic-title">Найди пять героев из сказок А. С. Пушкина</div>'
    html += '<img class="main-pic" src="' + appOptions.cdnPath + 'room.png">'
    html += '<img class="hidden-object" data-id="1" src="' + appOptions.cdnPath + 'fish.png">'
    html += '<div class="hidden-object-name" data-id="1">Золотая рыбка</div>'
    html += '<img class="hidden-object" data-id="2" src="' + appOptions.cdnPath + 'swan.png">'
    html += '<div class="hidden-object-name" data-id="2">Царевна Лебедь</div>'
    html += '<img class="hidden-object" data-id="3" src="' + appOptions.cdnPath + 'cat.png">'
    html += '<div class="hidden-object-name" data-id="3">Кот учёный</div>'
    html += '<img class="hidden-object" data-id="4" src="' + appOptions.cdnPath + 'clock.png">'
    html += '<div class="hidden-object-name" data-id="4">Золотой петушок</div>'
    html += '<img class="hidden-object" data-id="5" src="' + appOptions.cdnPath + 'mirror.png">'
    html += '<div class="hidden-object-name" data-id="5">Говорящее зеркальце</div>'
    html += '</div>'
    html += '<div class="hidden-object-footer"></div>'
    html += '</div>'

    that.drawTestModal(function() {
      Modal.close();
      $('.main-container').empty().append(html);
      $('.hidden-object').on('click', function () {
        if (!$(this).hasClass('hidden-object--selected')) {
          $(this).addClass('hidden-object--selected');
          $(this).next('.hidden-object-name').addClass('hidden-object-name--selected')
          that.score++;
          $('.counter').html(that.score + '/5');
          if (that.score === 5) {
            Timer.stop();
          }
        }
      })
      Timer.maxTime = 1200;
      Timer.callback = function() {
        if (typeof that.callback === 'function') {
          that.callback({
            score: that.getScore(),
            time: Timer.getTime(),
            id: that.test.id
          });
        }
      }
      Timer.start();
    })
  },

  drawPortrets: function() {
    var that = this,
      html = '';

    html += '<div class="counter">0/3</div>'
    html += '<div class="potrtrait-container">'
    html += '<div class="potrtrait-content">'
    html += '<div class="potrait-title">' + that.test.desc + '</div>'
    html += '<img class="portrait" data-id="1" src="' + appOptions.cdnPath + 'p1.png">'
    html += '<img class="portrait" data-id="2" src="' + appOptions.cdnPath + 'p2.png">'
    html += '<img class="portrait" data-id="3" src="' + appOptions.cdnPath + 'p3.png">'
    html += '<img class="portrait" data-id="4" src="' + appOptions.cdnPath + 'p4.png">'
    html += '<img class="portrait" data-id="5" src="' + appOptions.cdnPath + 'p5.png">'
    html += '<img class="portrait" data-id="6" src="' + appOptions.cdnPath + 'p6.png">'
    html += '<img class="portrait" data-id="7" src="' + appOptions.cdnPath + 'p7.png">'
    html += '<img class="portrait" data-id="8" src="' + appOptions.cdnPath + 'p8.png">'
    html += '<img class="portrait" data-id="9" src="' + appOptions.cdnPath + 'p9.png">'
    html += '<img class="portrait" data-id="10" src="' + appOptions.cdnPath + 'p10.png">'
    html += '</div>'
    html += '</div>'
    html += '<div class="hidden-object-footer"></div>'

    that.drawTestModal(function() {
      Modal.close();
      $('.main-container').empty().append(html);
      that.miss = 0;
      $('.portrait').on('click', function () {
        if ($(this).hasClass('checked')) {
          return;
        }
        var imgSrc = '',
          offset = $(this).offset();

        $(this).addClass('checked')
        if (Content.portraitIds.indexOf($(this).data('id')|0) > -1) {
          that.score++;
          $('.counter').html(that.score + '/3');
          imgSrc = appOptions.cdnPath + 'correct.png'
        } else {
          that.miss++;
          imgSrc = appOptions.cdnPath + 'wrong.png'
        }
        var html = ''

        html += '<img class="portrait-check" src="' + imgSrc + '" style="position:absolute;top:' + offset.top
        html += 'px;left:' + offset.left + 'px;">'
        $('.main-container').append(html);
        if (that.score === 3) {
          Timer.stop();
        }
      })

      Timer.maxTime = 1200;
      Timer.callback = function() {
        if (typeof that.callback === 'function') {
          that.callback({
            score: that.getScore(),
            time: Timer.getTime(),
            id: that.test.id
          });
        }
      }
      Timer.start();
    })
  },

  drawTest: function() {
    var that = this;

    that.drawTestModal(function() {
      that.drawNextQuestion();

      Timer.maxTime = 1200;
      Timer.callback = function() {
        if (typeof that.callback === 'function') {
          that.callback({
            score: that.getScore(),
            time: Timer.getTime(),
            id: that.test.id
          });
        }
      }
      Timer.start();
    })
  },

  drawNextQuestion: function () {
    var html = '',
      that = this,
      question = that.getNextQuestion();

    if (void 0 === question) {
      Timer.stop();

      return;
    }
    html += '<div class="test-title">Тест</div>'
    html += '<div class="test-desc">' + that.test.desc + '</div>'
    html += '<div class="test-item">' + question.text + '</div>'
    html += '<div class="test-answer-container">'
    html += '<button class="test-answer test-answer--correct" data-val="1">' + that.test.answerCorrect + '</button>'
    html += '<button class="test-answer test-answer--wrong" data-val="0">' + that.test.answerWrong + '</button>'
    html += '</div>'
    html += '<div class="answer-check"></div>'

    $('.modal-content').fadeOut()
    setTimeout(function() {
      $('.modal-content').empty().html(html).fadeIn();
      Modal.setCenter();

      $('.test-answer').on('click', function() {
        var val = $(this).data('val')|0;

        if (that.check(val)) {
          $('.answer-check').addClass('answer-check--correct').html('Верно!');
          that.score++;
        } else {
          $('.answer-check').addClass('answer-check--wrong').html('Неверно')
        }
        setTimeout(function() {
          that.drawNextQuestion();
        }, 500)
      });
    }, 400);
  },

  getNextQuestion: function() {
    this.currentQuestion++;

    return this.model[this.currentQuestion];
  },

  check: function(answer) {
    return this.model[this.currentQuestion].correct === answer;
  },

  getScore: function () {
    var result = 0;

    switch (this.test.id) {
      case 1:
        var time = Timer.getTime();

        if (time <= 30) {
          result = 10;
        } else if (time > 30 && time < 41) {
          result = 8;
        } else if (time > 41 && time <= 60) {
          result = 5;
        } else {
          result = 3;
        }
        break;
      case 2:
      case 3:
        result = this.score;
        break;
      case 4:
        if (this.miss === 0) {
          result = 10;
        } else if (this.miss >= 1 && this.miss < 3) {
          result = 8;
        } else if (this.miss >= 3 && this.miss < 5) {
          result = 5;
        } else {
          result = 3;
        }
        break;
    }

    return result;
  },

  getQuestionById: function(id) {
    var data = this.model;

    for (var i = 0; i < data.length; i++) {
      if (data[i].id == id) {
        return data[i]
      }
    }

    return {};
  }
}

export { Test }
